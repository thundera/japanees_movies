from django.apps import apps
from rest_framework import status, viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import FilmSerializerWithPeople
from .models import Film
from .db_updater import db_updater


class FilmViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Film.objects.all().order_by('release_date')
    serializer_class = FilmSerializerWithPeople

    def list(self, request, *args, **kwargs):
        db_updater.update()
        return super().list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        db_updater.update()
        return super().retrieve(request, *args, **kwargs)


class HealthView(APIView):

    def get(self, request):
        return Response(
            {'message': 'It is working!'},
            status=status.HTTP_200_OK
        )
