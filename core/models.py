from django.db import models


GENDERS = [
    ('M', 'Male'),
    ('F', 'Female'),
]


class Film(models.Model):
    id = models.UUIDField(primary_key=True)
    title = models.CharField(max_length=100)
    description = models.TextField()
    director = models.CharField(max_length=100)
    producer = models.CharField(max_length=100)
    release_date = models.CharField(max_length=100)
    rt_score = models.CharField(max_length=100)
    people = models.ManyToManyField('Character')


class Character(models.Model):
    id = models.UUIDField(primary_key=True)
    name = models.CharField(max_length=100)
    gender = models.CharField(max_length=100, blank=True)
    age = models.CharField(max_length=100, blank=True)
    eye_color = models.CharField(max_length=100, blank=True)
    hair_color = models.CharField(max_length=100, blank=True)
