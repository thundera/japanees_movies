from rest_framework import serializers
from .models import Film, Character

__all__ = ['FilmSerializer', 'CharacterSerializer', 'FilmSerializerWithPeople']


class CharacterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Character
        fields = [
            "id",
            "name",
            "gender",
            "age",
            "eye_color",
            "hair_color"
        ]


class FilmSerializer(serializers.HyperlinkedModelSerializer):
    people = CharacterSerializer(read_only=True, many=True)

    class Meta:
        model = Film
        fields = [
            "id",
            "title",
            "description",
            "director",
            "producer",
            "release_date",
            "rt_score",
        ]


class FilmSerializerWithPeople(FilmSerializer):
    people = CharacterSerializer(read_only=True, many=True)

    class Meta(FilmSerializer.Meta):
        FilmSerializer.Meta.fields.append("people")
