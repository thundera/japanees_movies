from django.urls import include, path
from rest_framework import routers
from .views import FilmViewSet, HealthView

router = routers.DefaultRouter()
router.register(r'films', FilmViewSet)


urlpatterns = [
    path('health', HealthView.as_view()),
    path('', include(router.urls))
]
