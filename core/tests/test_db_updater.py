from datetime import datetime, timedelta
from uuid import UUID
from django.test import TestCase
from django.conf import settings
from unittest import mock
from core.db_updater import db_updater
from core.models import Film, Character
from core.serializers import FilmSerializer
from .sample_data import one_film, some_people


# This method will be used by the mock to replace requests.get
def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

    if args[0] == 'https://ghibliapi.herokuapp.com/films':
        return MockResponse(one_film, 200)
    elif args[0] == 'https://ghibliapi.herokuapp.com/people':
        return MockResponse(some_people, 200)

    return MockResponse(None, 404)


class TestUpdater(TestCase):
    def setUp(self) -> None:
        db_updater.last_updated = datetime.utcnow() - timedelta(minutes=300)

    @mock.patch('core.db_updater.requests.get', side_effect=mocked_requests_get)
    def test_update_stale_database(self, mocked_get):
        # set last_updated to 5 minutes
        db_updater.last_updated = datetime.utcnow() - timedelta(minutes=300)
        # try to update the database
        db_updater.update()
        # expect two calls to API (films and people)
        self.assertEqual(mocked_get.call_count, 2)

    @mock.patch('core.db_updater.requests.get', side_effect=mocked_requests_get)
    def test_dont_update_fresh_database(self, mocked_get):
        # set last_updated to now
        db_updater.last_updated = datetime.utcnow()
        # try to update the database
        db_updater.update()
        # don't expect the database to be updated
        self.assertEqual(mocked_get.call_count, 0)

    @mock.patch('core.db_updater.requests.get', side_effect=mocked_requests_get)
    def test_update_films(self, mocked_get):
        film_id = one_film[0]['id']
        self.assertEqual(0, Film.objects.filter(id=UUID(film_id)).count())

        db_updater.update()

        self.assertEqual(1, Film.objects.count())
        self.assertIsNotNone(Film.objects.get(id=UUID(film_id)))

    def test_updated_relationships_correctness(self):
        pass

    def test_films_updated_correctly(self):
        pass

    def test_people_updated_correctly(self):
        pass

    def test_update_only_new_entries(self):
        pass
