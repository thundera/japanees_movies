import datetime
from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'core'
    last_updated = datetime.datetime.utcnow() - datetime.timedelta(seconds=600)
