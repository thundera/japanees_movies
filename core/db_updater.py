import requests
import datetime
import logging
import uuid
from django.conf import settings
from django.apps import apps
from .models import Film, Character
from .serializers import FilmSerializer, CharacterSerializer


logger = logging.getLogger(__name__)


class DBUpdater:

    @property
    def last_updated(self):
        app = apps.get_app_config("core")
        return app.last_updated

    @last_updated.setter
    def last_updated(self, value):
        app = apps.get_app_config("core")
        app.last_updated = value

    def update(self):
        now = datetime.datetime.utcnow()
        if now - self.last_updated > datetime.timedelta(
                seconds=settings.UPDATE_PERIOD):
            self.last_updated = now
            new_films = self.update_films()
            new_people = self.update_people()

            self._update_relationships(new_films, new_people)

    def update_films(self):
        return self._update_entity(
            settings.FILMS_URL, Film, FilmSerializer)

    def update_people(self):
        return self._update_entity(
            settings.PEOPLE_URL, Character, CharacterSerializer)

    def _update_relationships(self, new_films, new_people):
        if new_films or new_people:
            for character_dict in new_people:
                character_obj = Character.objects.get(
                    pk=uuid.UUID(character_dict["id"]))
                for film in character_dict['films']:
                    film_id = film.split('/')[-1]
                    film_obj = Film.objects.filter(pk=uuid.UUID(film_id))
                    if film_obj:
                        film_obj.people.add(character_obj)

    def _update_entity(self, url, model, serializer):
        response = requests.get(url)
        if response.status_code != 200:
            print(response)
            print(response.status_code)
            logger.warning('Something wrong with API')
            return []
        received_data = response.json()
        ids_to_update = {
            person["id"] for person in received_data
        }.difference({
            str(entry.id) for entry in model.objects.all().only('id')
        })
        if ids_to_update:
            new_films = list(
                filter(
                    lambda data: data["id"] in ids_to_update, received_data
                )
            )
            serialized = serializer(data=new_films, many=True)
            if serialized.is_valid():
                serialized.save()
            else:
                logger.error(
                    f"Something goes wrong. We should not have got here. "
                    f"Message: {serialized.error_messages}")
        return received_data


db_updater = DBUpdater()
